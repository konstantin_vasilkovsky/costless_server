var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mysql = require("mysql");
var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "costless"
});

var expressJwt = require('express-jwt');
var jwt = require('jsonwebtoken');
//TODO: MD5 secret
var SECRET = '09qrjjwef923jnrge$5ndjwk';
app.use('/api*',
	expressJwt({
		secret: SECRET,
		credentialsRequired: true,
		getToken: function fromHeaderOrQuerystring (req) {
			if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
			    return req.headers.authorization.split(' ')[1];
			} else if (req.query && req.query.token) {
			  return req.query.token;
			}
			return null;
		}
	})
);

app.set('port', 3030);
app.use(bodyParser.json()); 
app.use(bodyParser.urlencoded({
	extended: true
}));

con.connect(function(err){
  if(err){
    console.log('Error connecting to Db');
    return;
  }
  console.log('Connection established');
});

// con.end(function(err) {
  // The connection is terminated gracefully
  // Ensures all previously enqueued queries are still
  // before sending a COM_QUIT packet to the MySQL server.
// });

//REG NEW USER
app.post('/register', function (req, res) {
	//TODO: fields validation 
	var email = req.query.email,//r
		firstName = req.query.first_name,
		lastName = req.query.last_name,
		password = req.query.password,//r
		accountTypeId = req.query.accountTypeId,
		dataTypeId = req.query.dataTypeId,
		accountLevel = req.query.accountLevel,
		sex = req.query.gender,
		birthday = req.query.birthday;
	if(!email || !password){
		res.json({ success: false, message: "Email or Password missed"});
	}
	con.query('INSERT INTO users(first_name, last_name, sex, birthday, email, password) VALUES (?, ?, ?, ?, ?, ?)', [firstName,lastName,sex,birthday,email,password],function(err,rows){
	  if(err){
	  	res.json({ success: false });
	  	throw err
	  } else res.json({ success: true});
	});
});

//LOGIN USER
app.post('/login', function (req, res) {
	var email = req.query.email,
		pass = req.query.password;
	con.query('SELECT id, first_name, last_name, email, password FROM users WHERE email = ?', [email],function(err,rows){
	  if(err){
	  	res.json({ success: false, message: err});
	  	throw err
	  }
	  if(rows[0] && rows[0].password === pass){
		var profile = {
		    first_name: rows[0].first_name,
		    last_name: rows[0].last_name,
		    email: rows[0].email,
		    id: rows[0].id
		};
		var token = jwt.sign(profile, SECRET, { expiresIn: 60*60*60 });
	  	res.json({ token: token });
	  } else{
	  	res.json({ success: false, message: 'Invalid email or login'});
	  } 
	});
});

//LOGOUT USER
app.get('/logout', function (req, res) {
	req.user = null;
	req.query.token = '';
	res.json({ success: true });
});

/* 
	FIND PRODUCTS
	PARAMS: longitude, latitude, radius
	RETURN: Array 
*/
app.get('/api/findProducts', function (req, res) {
	con.query("SELECT * FROM products WHERE name = ?", [req.query.search],function(err,rows){
	  if(err || !rows[0]){
	  	res.json({ success: false, message: err});
	  	throw err
	  }
	  if(rows[0]){
	  	res.json({ success: true, data: rows});
	  }
	});
});

/* 
	GET SHOPS IN RADIUS
	PARAMS: longitude, latitude, radius
	RETURN: Array
*/
app.get('/api/getShopsInRad', function (req, res, next) {
	con.query("SELECT id,  ( 6371 * acos( cos( radians(34.34) )  * cos( radians( "+ req.query.latitude + " ) )  * cos( radians( "+req.query.longitude+" ) - radians(34.45) )  + sin( radians(34.34) ) * sin( radians( "+ req.query.latitude + " ) ) ) ) AS distance FROM shops HAVING distance < "+req.query.radius + " ORDER BY distance LIMIT 25" ,function(err,rows){
		if(err){
	  		res.json({ success: false });
	  		next();
	  	}
	  	res.json({ success: true, data: rows});
	  	next();
	});
});

/* 
	GET SHOP THAT HAVE PRODUCT
	PARAMS: prodID, longitude, latitude, radius
	RETURN: Array 
*/
app.get('/api/getShopsWithProd', function (req, res) {
	var shopIDs = [];
	con.query("SELECT id,  ( 6371 * acos( cos( radians(34.34) )  * cos( radians( "+ req.query.latitude + " ) )  * cos( radians( "+req.query.longitude+" ) - radians(34.45) )  + sin( radians(34.34) ) * sin( radians( "+ req.query.latitude + " ) ) ) ) AS distance FROM shops HAVING distance < "+req.query.radius + " ORDER BY distance LIMIT 25" ,function(err,rows){
	  if(err){
	  	res.json({ success: false });
	  	throw err
	  }
	  if(rows[0]){
	  	shopIDs = rows.map(function(s){
	  		return s.id;
	  	});
		var sql = 
				"SELECT DISTINCT products_in_shops.shop_id, shops.name, shops.latitude, shops.longitude, shops.type_id,"
				+"shops.icon, shops.address FROM  products_in_shops, shops "
				+"WHERE  products_in_shops.shop_id =  shops.id AND products_in_shops.product_id= ? AND shops.id IN("+shopIDs.join()+")";
		con.query(sql, [req.query.productId],function(err,rows){
		  if(err){
		  	res.json({ success: false, message: err});
		  	throw err
		  }
		  if(rows[0]){
		  	res.json({ success: true, data: rows});
		  }
		});
	  }
	});
});

/* 
	GET PRODUCT PRICE
	PARAMS: productId, shopId
	RETURN: Obj 
*/
app.get('/api/getProductPrice', function(req, res){
	var sql = "SELECT price FROM products_in_shops WHERE product_id=? AND shop_id=? AND "
			+ "updated_at=(SELECT MAX(updated_at) FROM products_in_shops WHERE product_id= ? AND shop_id= ? '";
	con.query(sql, [req.query.productId, req.query.shopId, req.query.productId, req.query.shopId],function(err,rows){
	  if(err){
	  	res.json({ success: false, });
	  	throw err
	  }
	  if(rows[0]){
	  	res.json({ success: true, data: rows[0]});
	  }
	});
});

app.listen(3030);
